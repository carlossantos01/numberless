import { Component } from "react";
import SearchBar from "./components/SearchBar";
import "./App.css";

class App extends Component {
  state = {
    status: "",
    cases: [],
  };
  updateBrazilianState(estadoAtual) {
    this.setState = { brazilianstate: estadoAtual };
  }
  componentDidMount() {
    fetch("https://covid19-brazil-api.now.sh/api/report/v2")
      .then((res) => res.json())
      .then((res) => {
        return res.data.map((data) => {
          this.setState({ cases: [...this.state.cases, data.cases] });
        });
      });
  }
  render() {
    return (
      <>
        <SearchBar func={this.updateBrazilianState} />
        <div>{this.state.cases[0]}</div>
      </>
    );
  }
}

export default App;
